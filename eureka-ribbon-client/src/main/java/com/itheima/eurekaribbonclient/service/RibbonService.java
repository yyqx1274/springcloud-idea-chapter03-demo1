package com.itheima.eurekaribbonclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by itcast on 2019/10/18.
 */
@Service
public class RibbonService {
    @Autowired
    RestTemplate restTemplate;
    public String hi(){
        return restTemplate.getForObject
                ("http://eureka-provider/user/findAll",String.class);
    }
    public String hicustomer(){
        return restTemplate.getForObject
                ("http://eureka-provider/customers",String.class);
    }
}