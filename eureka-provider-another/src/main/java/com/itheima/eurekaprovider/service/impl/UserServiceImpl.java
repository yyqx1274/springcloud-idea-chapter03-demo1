package com.itheima.eurekaprovider.service.impl;

import com.itheima.eurekaprovider.dao.UserDao;
import com.itheima.eurekaprovider.pojo.User;
import com.itheima.eurekaprovider.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;
    @Override
    public User login(String username, String password) {
        User user = userDao.findByUser(username,password);
        return user;
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

}
