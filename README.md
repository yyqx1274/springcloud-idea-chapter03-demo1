# springcloud-idea-chapter03-a  Ribbon客户端负载均衡

小组成员：（2-5人）

学号+姓名

角色

一、实验目的
高可用注册中心实验

二、实验仪器设备与环境

1、Eureka Server（1个）

2、Provider （2个）

3、Consumer （1个）

三、实验原理

Eureka Server + Provider + Consumer（Ribbon）

![](./doc/x.png)

四、实验内容与步骤

配置Maven环境

新建项目

创建码云项目

小组克隆项目

根据角色修改配置文件

五、实验结果与分析

最终实验结果界面

当Provider1或者Provider2停机后，访问Consumer会有什么效果？

六、结论与体会

通过这次实验，理解了高可用。