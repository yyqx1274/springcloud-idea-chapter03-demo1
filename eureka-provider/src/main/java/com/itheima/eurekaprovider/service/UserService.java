package com.itheima.eurekaprovider.service;

import com.itheima.eurekaprovider.pojo.User;

import java.util.List;


public interface UserService {
    public User login(String username, String password);
    public List<User> findAll();
}
